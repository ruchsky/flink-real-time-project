package com.atguigu.gmall.realtime.utils;

import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;

import java.util.Properties;

public class MyKafka {

    //封装kafka消费者类
    //方法需要传入参数为，消费的话题，消费者组
    public static FlinkKafkaConsumer getFlinkKafkaConsumer(String topic,String groupId){

        Properties properties=new Properties();//配置
        //连接kafka的连接地址
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"hadoop104:9092,hadoop105:9092,hadoop106:9092");
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG,"groupId");
        return new FlinkKafkaConsumer(topic,new SimpleStringSchema(),properties);
    }
}
