package com.atguigu.gmall.realtime.bean;

import lombok.Data;
//TODO 将监控的配置表封装成一个对象.所谓配置表，[就是将maxwell动态监控的mysql业务数据写从kafka中，若是事实表
//todo 写入kafka的topic,若是维度表写入，hbase,此配置表就是动态的表示topic流入的表是哪一种类型，需手动维护.]

@Data
public class TableProcess {
    //动态分流Sink常量   改为小写和脚本一致
    public static final String SINK_TYPE_HBASE = "hbase";
    public static final String SINK_TYPE_KAFKA = "kafka";
    public static final String SINK_TYPE_CK = "clickhouse";
    //来源表
    String sourceTable;
    //操作类型 insert,update,delete
    String operateType;
    //输出类型 hbase kafka
    String sinkType;
    //输出表(主题)
    String sinkTable;
    //输出字段
    String sinkColumns;
    //主键字段
    String sinkPk;
    //建表扩展
    String sinkExtend;

}
