package com.atguigu.gmall.realtime.app.func;

import com.alibaba.fastjson.JSONObject;

import java.text.ParseException;

//维度关联需要实现的接口,需要去实现两个方法，获取维度数据的key,join方法
public interface DimJoinFunction<T> {
    //获取维度数据的key的方法
    String getKey(T Obj);
    //维度数据的关联
    void join(T obj, JSONObject dimInfoJsonObj) throws ParseException;

}
