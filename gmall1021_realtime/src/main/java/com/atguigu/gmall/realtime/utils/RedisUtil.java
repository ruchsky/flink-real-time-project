package com.atguigu.gmall.realtime.utils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

//获取redis来连接池的客户端
public class RedisUtil {
    //定义一个连接池
    private static JedisPool jedisPool;
    public static Jedis getJedis(){
        //如果连接池为空，创建一个连接池
        if(jedisPool == null){
            JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
            //最大可用连接数
            jedisPoolConfig.setMaxIdle(5);
            //最小可以用连接数
            jedisPoolConfig.setMinIdle(5);
            //连接耗尽是否等待
            jedisPoolConfig.setBlockWhenExhausted(true);
            //等待时间，这个是已经连接上了，等待时间
            jedisPoolConfig.setMaxWaitMillis(2000);
            //可以写入配置，来配置条件，时间为来凝结redis等待时间
             jedisPool = new JedisPool(jedisPoolConfig,"hadoop104",6379,10000);
            System.out.println("连接池开辟成功");
        }
        return jedisPool.getResource();
    }

    public static void main(String[] args) {
        Jedis jedis = getJedis();
        String msg = jedis.ping();
        System.out.println(msg);
    }
 }
