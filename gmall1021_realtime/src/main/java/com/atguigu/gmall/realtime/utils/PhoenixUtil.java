package com.atguigu.gmall.realtime.utils;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.gmall.realtime.common.GmallConfig;
import org.apache.commons.beanutils.BeanUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

//封装一个phoenix工具类，用来连接和操作phoenix
public class PhoenixUtil {
    //在外面定义连接对象，可以在下面使用
    private static  Connection conn;
    //TODO 1 在方法中，创建连接的对象
    public static void initConnection(){
        try {
            //1.1 注册驱动
            Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");
            //1.2 获取连接
            conn = DriverManager.getConnection(GmallConfig.PHOENIX_SERVER);
            //1.3 设置操作表的空间
            conn.setSchema(GmallConfig.HBASE_SCHEMA);
            //System.out.println("初始化方法被调用，创建了对象conn"+conn);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //TODO 2 从phoenix中查询数据
    public static <T>List<T> queryList(String sql,Class<T> clz){
        //2.1 判断如果来连接对象为空，调用创建方法创建连接
        if(conn == null){
            initConnection();
            // System.out.println("第一次，没有创建，创建连接对象"+conn);
        }

        // 为了关闭连接对象，所以需要那个操作对象声明在外面
        PreparedStatement ps= null;
        ResultSet rs=null;
        //返回的结果为多条数据，所以可以用list集合储存起来
        List<T> resList = new ArrayList<>();
        try {
            System.out.println(conn+"*******");
            //2.2 创建数据库操作对象
            ps = conn.prepareStatement(sql);
            // System.out.println("获取操作对象为:"+ps);
            //2.3 执行sql语句
            rs = ps.executeQuery();

            // System.out.println("返回的结果集为+"+rs);
            //返回的结果集形式
            /*  ID,TN_NAME
             *    14 测试数据
             *    15 ce
             * */

            //获取返回的结果集的元数据信息，通过元数据信息，可以获取结果集中的列名和列的数量
            ResultSetMetaData metaData = rs.getMetaData();
            //获取元数据的列数量，可以用来遍历列明
            int columnCount = metaData.getColumnCount();
            //System.out.println("列数量为+"+columnCount);

            //当有数据时，一直进行处理
            while(rs.next()){
                //创建要封装成为的对象
                T obj = clz.newInstance();
                for(int i=1; i<=columnCount;i++){
                    //获取每个列名
                    String columnName = metaData.getColumnName(i);
                    //System.out.println("获取的列名为:"+columnName);
                    //将每个给对象的属性赋值
                    BeanUtils.setProperty(obj,columnName,rs.getObject(i));
                }
                resList.add(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            //2.2 先关闭结果集
            if(rs != null){
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            //2.3 关闭操作对象
            if(ps != null){
                try {
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return resList;

    }

    public static void main(String[] args) {
        //TODO 3 调用查询方法
        List<JSONObject> list = queryList("select * from GMALL1021_REALTIME.BASE_TRADEMARK", JSONObject.class);
        System.out.println(list);
        /*获取的数据为
         * [{"ID":"14","TM_NAME":"测试插入"}, {"ID":"15","TM_NAME":"ce"}]
         * */

    }

}
