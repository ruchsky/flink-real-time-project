package com.atguigu.gmall.realtime.utils;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

//封装一个线程池工具类，使得有多个redis客户端效果，提高效率
public class ThreadPoolUtil {
    //创建对象
    private static ThreadPoolExecutor pool;

    public  static ThreadPoolExecutor getInstance(){
    if(pool ==null){

        synchronized (ThreadPoolUtil.class){

            //如果线程池为空，则创建
            /*
             * 1.初始线程的数量
             * 2.最大线程的数量,
             * 3.空闲线程超过多久会被销毁
             * 4.队列
             * 1*/
            if(pool==null){
                pool = new ThreadPoolExecutor(
                        4,
                        20,
                        10,
                        TimeUnit.SECONDS,
                        new LinkedBlockingDeque<Runnable>(Integer.MAX_VALUE)

                );
            }
        }
    }
        return pool;
    }
}
