package com.atguigu.gmall1021.gmall1021_logger.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Slf4j
public class LonggerController {
    @Resource
    private KafkaTemplate<String,String> kafkaTemplate;



   // private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(LonggerController.class);

    @RequestMapping("/applog")
    public String getLongger(@RequestParam("param") String jsonStr){
        //TODO 1.打印到控制台
        //System.out.println(jsonStr);
        //TODO 2.日志落盘
        log.info(jsonStr);
        //TODO 3.发送到kafka
        kafkaTemplate.send("ods_base_log",jsonStr);



        return "success";



    }

}
